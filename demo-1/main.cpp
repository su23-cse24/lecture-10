#include<iostream>
using namespace std;

int main() {

    int** p = new int*[5];

    for (int i = 0; i < 5; i++) {
        // p[i] = new int[5];
        *(p + i) = new int[5];
    }

    p[0][0] = 1;
    p[1][1] = 3;
    // p[2][2] = 5;
    *(*(p + 2) + 2) = 5;
    p[3][3] = 7;
    p[4][4] = 9;

    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++) {
            // cout << p[i][j] << " ";
            cout << *(*(p + i) + j) << " ";
        }
        cout << endl;
    }

    for (int i = 0; i < 5; i++) {
        delete[] p[i];
    }

    delete[] p;

    return 0;
}