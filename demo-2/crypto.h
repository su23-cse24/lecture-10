#ifndef CRYPTO_H
#define CRYPTO_H

#include <string>
#include <iostream>

std::string decode(long s){
    std::string result = "";

    void* p = &s;

    for (int i = 0; i < 8; i++) {
        char letter = *(char*)p;
        result += letter;
        p = (char*)p + 1;
    }   

    return result;
}

#endif