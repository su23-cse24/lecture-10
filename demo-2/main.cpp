#include <iostream>
#include "crypto.h"

using namespace std;

int main(int argc, char* argv[]){
	// Your code here

    if (argc < 2){
        cerr << "Not enough arguments" << endl;
        return 1;
    }

    long secret = stol(argv[1]);

    string message = decode(secret);

    cout << message << endl;

	return 0;
}
