#include<iostream>
using namespace std;

void append(unsigned char* stash, char* types, int& pos, int& size, int x) {
    unsigned char* temp = stash;
    temp += pos;

    *(int*)temp = x;
    types[size] = 'i';

    pos += sizeof(int);
    size++;
}

void append(unsigned char* stash, char* types, int& pos, int& size, char x) {
    unsigned char* temp = stash;
    temp += pos;

    *temp = x;
    types[size] = 'c';

    pos += sizeof(char);
    size++;
}

void append(unsigned char* stash, char* types, int& pos, int& size, float x) {
    unsigned char* temp = stash;
    temp += pos;

    *(float*)temp = x;
    types[size] = 'f';

    pos += sizeof(float);
    size++;
}



void print(unsigned char* stash, char* types, int& size) {
    unsigned char* temp = stash;

    cout << "[ ";
    for (int i = 0; i < size; i++) {
        if (types[i] == 'i') {
            cout << *(int*)(temp) << " ";
            temp += sizeof(int);
        } else if (types[i] == 'c') {
            cout << *(temp) << " ";
            temp += sizeof(char);
        } else if (types[i] == 'f') {
            cout << *(float*)(temp) << " ";
            temp += sizeof(float);
        }
    }
    cout << "]" << endl;;
}

int main() {

    unsigned char* stash = new unsigned char[10];
    char types[3];
    int pos = 0;
    int size = 0;

    append(stash, types, pos, size, 42);
    append(stash, types, pos, size, 'A');
    append(stash, types, pos, size, 3.5f);
    print(stash, types, size);

    delete[] stash;

    return 0;
}