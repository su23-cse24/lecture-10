#include<iostream>
using namespace std;

void append(void **stash, char* types, int& size, int x) {
    stash[size] = new int(x);
    types[size] = 'i';
    size++;
}

void append(void **stash, char* types, int& size, char x) {
    stash[size] = new char(x);
    types[size] = 'c';
    size++;
}

void append(void **stash, char* types, int& size, float x) {
    stash[size] = new float(x);
    types[size] = 'f';
    size++;
}

void print(void **stash, char* types, int& size) {
    cout << "[ ";
    for (int i = 0; i < size; i++) {
        if (types[i] == 'i') {
            cout << *(int*)stash[i] << " ";
        } else if (types[i] == 'c') {
            cout << *(char*)stash[i] << " ";
        } else if (types[i] == 'f') {
            cout << *(float*)stash[i] << " ";
        }
    }
    cout << "]" << endl;
}


int main() {

    void **stash = new void*[4];
    char types[3];
    int size = 0;

    append(stash, types, size, 42);
    append(stash, types, size, 'A');
    append(stash, types, size, 3.5f);

    print(stash, types, size);

    for (int i = 0; i < size; i++) {
        if (types[i] == 'i') {
            delete[] (int*)stash[i];
        } else if (types[i] == 'c') {
            delete[] (char*)stash[i];
        } else if (types[i] == 'f') {
            delete[] (float*)stash[i];
        }
    }
    delete[] stash;

    return 0;
}